﻿using System;
using System.IO;
using System.Linq;

class Task1545
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"6
na
no
ni
ki
ka
ku
k");
        var a = input.ReadToEnd().Split('\n');
        int n = int.Parse(a[0]);
        char target = a[n+1][0];
        for (int i = 1; i <= n; i++)
        {
            if (a[i][0] == target) Console.WriteLine(a[i]);
        }
      
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
