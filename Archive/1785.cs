﻿using System;
using System.IO;
using System.Linq;

class Task1785
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(@"4") : Console.In;
        int n = int.Parse(input.ReadLine());
        var b = "few,several,pack,lots,horde,throng,swarm,zounds,legion".Split(',');
        var a = new[] {4, 9, 19, 49, 99, 249, 499, 999};
        Console.WriteLine(b[Result(n,a)]);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }

    private static int Result(int n, int[] a)
    {
        return a.Count(x => x < n);
    }
}