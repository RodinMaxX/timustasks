﻿using System;
using System.IO;

class Task2031
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"5
");
        int n = int.Parse(input.ReadLine());
        if (n < 5) Console.WriteLine("16 06 68 88".Substring(0, n*3 - 1));
        else Console.WriteLine("Glupenky Pierre");
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}