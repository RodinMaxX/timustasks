﻿using System;
using System.IO;

class Task1712
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"....
X..X
.X..
...X
Pwoo
Khaa
smrs
odbk
");
        int n = 4;
        char[,] m = new char[n,n];
        for (int i = 0; i < n; i++)
        {
            var t = input.ReadLine();
            for (int j = 0; j < n; j++)
            {
                m[i,j] = t[j];
            }
        }
 
        var s = input.ReadToEnd().Split('\n');
        for (int i = 0; i < n; i++)
        {
            Print(m,s);
            m=Rotate(m);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }

    private static char[,] Rotate(char[,] m1)
    {
        int n = m1.GetUpperBound(0) + 1;
        char[,] m2 = new char[n, n];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                m2[i, j] = m1[(n-1)-j, i];
            }
        }
        return m2;
    }

    private static void Print(char[,] mask, string[] s)
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                if (mask[i,j] =='X')
                Console.Write(s[i][j]); 
            }
        }
    }
}
