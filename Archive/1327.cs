﻿using System;
using System.IO;

class Task1327
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"100
200");

        var t0 = int.Parse(input.ReadLine());
        var t1 = int.Parse(input.ReadLine());
        
        decimal counter = ((t1 - t0 + 1 + (t0 % 2)) / 2); 

        Console.WriteLine(counter);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
