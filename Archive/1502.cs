﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

class Task1502
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"2
");
        int n = int.Parse(input.ReadLine());
        double sum = 0;
        for (int i = 1; i <=n; i++)
        {
            sum +=  (i * (i + 1)) / 2 + i * (i + 1);
        }
        Console.WriteLine(sum);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

        