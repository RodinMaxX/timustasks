﻿using System;
using System.IO;

class Task1404
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"xbduyr");

        var s = input.ReadLine().ToCharArray();
        for (int i = s.Length - 1; i >= 0; i--)
        {
            int b = (i > 0) ? (s[i - 1] - 'a') : 5;
            int a = s[i] - 'a';
            int c = a - b;
            if (c < 0) c += 26;
            s[i] =(char)(c +'a');
        }
        Console.WriteLine(s);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
