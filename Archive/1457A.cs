﻿using System;
using System.Globalization;
using System.Threading;

namespace _1457t
{
    class Program
    {
        static void Main(string[] args)
        
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            // Console.WriteLine("Введите количество точек: ");
            int q = int.Parse(Console.ReadLine());
            // Console.WriteLine("Введите 'магистральные смещения' (координаты) точек через пробел: ");
            string[] points = Console.ReadLine().Split(' ');
            // int[] point = new int[q];
            int summ = 0;
            for (int i = 0; i < q; i++)
            {
                // point[i] = int.Parse(points[i]);
                summ = summ + int.Parse(points[i]); // + point[i];
            }
            double r =(double)summ / q; 
            Console.WriteLine("{0:F6}", r);
            // Console.ReadLine(); 
        }
    }
}
