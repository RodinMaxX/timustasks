﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _1073
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input1.txt") : Console.In;

            int n = int.Parse(input.ReadLine());

            int[] a = new int[n+1];

            Console.WriteLine(Result(n,a));

            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
         
        static int Result(int n, int[] a)
        {
            if (n <= 1) return n;
            if (a[n] > 0) return a[n];
            int min=Int32.MaxValue;
            for (int i = 1; i*i <= n; i++)
            {
                int x;
                x = 1 + Result(n - i * i, a);
                min = Math.Min(x,min);
            }
            a[n] = min;
            return  min;
        }
    }
}
