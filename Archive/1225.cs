﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _1225
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0 ? new StringReader("46") : Console.In;
           
            int n = int.Parse(input.ReadLine());

          int[] a = new int[n + 1];

            Console.WriteLine(2*(uint)Result(n, a));

            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }

        static int Result(int n, int[] a)
        {
            if (n <= 2) return 1;
            if (a[n] > 0) return a[n];
            int r = Result(n - 2, a) + Result(n - 1, a);
            a[n] = r;
            return r;
        }

       /* static int SumWhite(int n)
        {
            if (n <= 2) return 1;
            int r = SumRed(n - 2) + SumRed(n - 1);
            return r;
        } */


    }
}
