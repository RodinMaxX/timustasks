﻿using System;
using System.IO;

class Task1139
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
                @"4 3
");
        var s = input.ReadLine().Split(' ');
        int n = int.Parse(s[0]);
        int m = int.Parse(s[1]);
        --n;
        --m;
        if (m == n)
        {
            Console.WriteLine(n);
        }
        else
            Console.WriteLine(n + m - NOD(m, n));
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }

    private static int NOD(int a, int b)
    {
        if (b == 0) return a;
        return NOD(b, a % b);
    }
}
