﻿using System;
using System.IO;

class Program_1493
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input2.txt") : Console.In;
        int n = int.Parse(input.ReadLine());
        Console.WriteLine(HappyNum(n-1) || HappyNum(n+1) ? "Yes" : "No");
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }

    static bool HappyNum(int i)
    {
        int a;
        int b = Math.DivRem(i, 1000, out a);
        return DigitsSum(a) == DigitsSum(b);
    }

    static int DigitsSum(int n)
    {
        int sum = 0;
        for (int x; n > 0; sum = sum + x)
        {
            n = Math.DivRem(n, 10, out x);
            ;
        }
        return sum;
    }
}

