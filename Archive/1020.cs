﻿using System;
using System.Threading;
//using System.Drawing;
using System.Globalization;
using System.IO;

namespace _1020
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input2.txt") : Console.In;

            PointF t =  GetPoint(input);
            int n = (int) t.X;
            double radius =  t.Y;
            //    int.Parse(input.ReadLine());
        //    int r = int.Parse(input.ReadLine());

            PointF a = GetPoint(input);
            PointF b = new PointF();
            b = a;
            double sumOfDist = 0;
            PointF p = a;
            for (int i = 1; i < n; i++)
            {
                b = GetPoint(input);
                var dist = Math.Sqrt(Math.Pow((b.Y - p.Y), 2) + Math.Pow((b.X - p.X), 2));
                p = b;
                sumOfDist += dist;
            }
            var lastDist = Math.Sqrt(Math.Pow((b.Y - a.Y), 2) + Math.Pow((b.X - a.X), 2));
            var result = sumOfDist + lastDist + 2 * radius * Math.PI;
            Console.WriteLine(result.ToString("f2"));
            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }

        static PointF GetPoint(TextReader input)
        {
            var a = input.ReadLine().Split(' ');
            double x = double.Parse(a[0]);
            double y = double.Parse(a[1]);
            PointF r = new PointF(x, y);
            return r;
        }


    }

    internal struct PointF
    {
        public double Y;
        public double X;
        public PointF(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
