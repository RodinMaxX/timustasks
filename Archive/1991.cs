﻿using System;

namespace _1991
{
    class Program
    {
        static void Main()
        {
            string[] nk = Console.ReadLine().Split(' ');
            int n = int.Parse(nk[0]);
            int k = int.Parse(nk[1]);
            int d = 0, b = 0;
            string[] a = Console.ReadLine().Split(' ');
            for (int i = 0; i < n; i++)
            {
                int x = (int.Parse(a[i]))- k;
                if (x > 0)
                {
                    b = b + x;
                }
                else
                {
                    d = d - x;
                }
                }

            Console.WriteLine ("{0} {1}", b, d);
        }
    }
}
