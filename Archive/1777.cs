﻿using System;
using System.IO;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader("11 5 9") : Console.In;
        long[] a = input.ReadLine().Split(' ').Select(long.Parse).ToArray();
        int i = 0;
        for (long min = long.MaxValue; a[2] != 0; i++)
        {
            Array.Sort(a);
            min = a[2] = Math.Min(min, Math.Min(Math.Abs(a[0] - a[1]), Math.Abs(a[1] - a[2])));
        }
        Console.WriteLine(i);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}