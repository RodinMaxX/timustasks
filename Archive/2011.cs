﻿using System;
using System.IO;

internal class Task2011
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
 @"6
1 2 2 3 3 3");
        int n = int.Parse(input.ReadLine());
        int[] counters = new int[3];
        int[] letters = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);

        for (int i = 0; i < 6; i++)
        {
            if (letters[i] == 1) counters[0]++;
            else if (letters[i] == 2) counters[1]++;
            else if (letters[i] == 3) counters[2]++;
        }
        Array.Sort(counters);
        if (counters[0] >= 2 & counters[1] >= 2 || counters[1] >= 2 & counters[2] >= 2) Console.WriteLine("Yes");
        else if (counters[0] == 1 || counters[1] == 1 || counters[2] == 1
                                                         & counters[0] == n - 1 || counters[1] == n - 1 ||
                 counters[2] == n - 1)
        {
            Console.WriteLine("Yes");
        }
        else if (counters[0] == 1 || counters[1] == 1 || counters[2] == 1)
        {
            Console.WriteLine("Yes");
        }
        else Console.WriteLine("No");


        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
