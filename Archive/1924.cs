﻿using System;
using System.IO;

class Task1924
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"3");
       
        var n = int.Parse(input.ReadLine());
        Console.WriteLine(("grimy,Автор,black").Split(',')[(n-1)&2]);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
