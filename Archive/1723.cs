﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class Task1723
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader (
@"tebidohtebidoh");

        var s = input.ReadLine();

        int[] counters = new int ['z'+1];

        for (int i = 0; i < s.Length; i++)
        {
            counters[s[i]]++;
        }

        var j = Array.LastIndexOf(counters, counters.Max());
        Console.WriteLine((char)j);

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
