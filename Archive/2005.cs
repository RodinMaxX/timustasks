﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task2005
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"0 2600 3800 2600 2500
2600 0 5300 3900 4400
3800 5300 0 1900 4500
2600 3900 1900 0 3700
2500 4400 4500 3700 0");

        int[,] dist = new int[5,5];

        for (int i = 0; i < 5; i++)
        {
            string[] lines = input.ReadLine().Split(' ');

            for (int j = 0; j < 5; j++)
            {
                dist[i, j] = int.Parse(lines[j]);
            }
        }

        var ways = new Dictionary<string,int>(4);
        var var1 = dist[0,1] + dist[1,2] + dist[2,3] + dist[3,4];
        ways.Add("1 2 3 4 5", var1);
        var var2 = dist[0, 2] + dist[2, 1] + dist[1, 3] + dist[3, 4];
        ways.Add("1 3 2 4 5", var2);
        var var3 = dist[0, 2] + dist[2, 3] + dist[3, 1] + dist[1, 4];
        ways.Add("1 3 4 2 5", var3);
        var var4 = dist[0, 3] + dist[3, 2] + dist[2, 1] + dist[1, 4];
        ways.Add("1 4 3 2 5", var4);
        var minKM = ways.Min(z => z.Value);
        Console.WriteLine(minKM);
        var key = ways.FirstOrDefault(x => x.Value == minKM).Key; // есть мнение,что нехорошо получать ключ на значение и такое надо переписывать..
        Console.WriteLine(key);


            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
    }

