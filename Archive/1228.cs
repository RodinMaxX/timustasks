﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace _1228
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args,"input")>=0 ? new StreamReader(@"..\..\input1.txt") : Console.In;
            string[] a = input.ReadLine().Split(' ');
            int n = int.Parse(a[0]);
            int s = int.Parse(a[1]);

            for (int i = 0; i < n; i++)
            {
                int block = int.Parse(input.ReadLine());
                int x = s/block - 1;
                s = block;
                Console.Write(x + " ");
            }

            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
    }
}
