﻿using System;
using System.IO;

namespace _1100
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            MyStruct[] a = new MyStruct[n];
            for (int i = 0; i < n; i++)
            {
                a[i] = new MyStruct(i,Console.ReadLine().Split(' '));
            }
            Array.Sort(a);

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(a[i].ToString());
            }

        }

        struct MyStruct : IComparable<MyStruct>
        {
            private int numb;
            private int teamID;
            private int tasksSolved;

            public MyStruct(int x, string[] t)
            {
                numb = x;
                teamID = int.Parse(t[0]);
                tasksSolved = int.Parse(t[1]);
            }

            public int CompareTo(MyStruct other)
            {
                var numbComparison = other.tasksSolved.CompareTo(tasksSolved);
                if (numbComparison != 0) return numbComparison;
                return numb.CompareTo(other.numb);
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", this.teamID, this.tasksSolved);
            }
        }

    }
}
