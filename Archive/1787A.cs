﻿using System;
using System.IO;
using System.Linq;

class Task1787A
{
    static void Main(string[] args)
    {
 
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(
@"5 3
20 0 0
"): Console.In;
        int carsPerMin = int.Parse(((input.ReadLine()).Split(' '))[0]);
        int r = 0;
        Array.ForEach(input.ReadLine().Split(' '), (string car) => r = Math.Max(0, r + int.Parse(car) - carsPerMin));
        Console.WriteLine(r);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }
}
