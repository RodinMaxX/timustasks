﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Task1370
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"2 60 600
600 637");
        var a = input.ReadLine().Split(' ');
        int n = int.Parse(a[0]);
        int t = int.Parse(a[1]);
        int s = int.Parse(a[2]);

        var sn =  input.ReadLine().Split(' ');
        var b = new[] {".000000", ".500000"};
        for (int i = 0; i < n; i++)
        {
            //var res = ((float)(t + s + int.Parse(sn[i])))/2;
            //Console.WriteLine("{0:f6}", res);
            var res = (t + s + int.Parse(sn[i]));
            Console.Write(res / 2);
            Console.WriteLine(b[res % 2]);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}