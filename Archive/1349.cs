﻿using System;
using System.IO;

class Task1349
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"3
");
        var n = int.Parse(input.ReadLine());
        if (n == 2)
        {
            Console.WriteLine("3 4 5");
        }
            else if (n == 1)
        {
           Console.WriteLine("1 2 3");
        }
        else
        {
            Console.WriteLine(-1);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
