﻿using System;
using System.IO;
using System.Linq;

class Task1787B
{
    static void Main(string[] args)
    {
 
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(
@"5 3
1 0 0
"): Console.In;
        int carsPerMin = int.Parse(((input.ReadLine()).Split(' '))[0]);
        var cars = input.ReadLine().Split(' ');

        int r = 0;
        for (int i = 0; i < cars.Length; i++)
        {
            r = Math.Max(0, r + int.Parse(cars[i]) - carsPerMin);
        }

         Console.WriteLine(r);
         if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }
}
