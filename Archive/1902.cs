﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1370
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"2 60 600
541 659");
        var a = input.ReadLine().Split(' ');
        int n = int.Parse(a[0]);
        int t = int.Parse(a[1]);
        int s = int.Parse(a[2]);

        int[] sn =  Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        int[] res = new int[n];
        for (int i = 0; i < n; i++)
        {
            res[i] = (t + s + sn[i])/2;
            Console.WriteLine("{0:f6}", res[i]);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

