﻿using System;
using System.IO;
using System.Linq;

class Task1877
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader("0001 \n 0000") : Console.In;

        int[] a = input.ReadToEnd().Split('\n').Take(2).Select(int.Parse).ToArray();
        Console.WriteLine("yes,no".Split(',')[a[0] * (a[1] + 1) % 2]);

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();

    }
}