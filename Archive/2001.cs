﻿using System;
using System.IO;
using System.Linq;

class Task2001
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(@"
2 2
3 1
0 4") : Console.In;
        string s = input.ReadToEnd();
        var n = (s.Split(' ', '\r', '\n'));
       
    int[] w = new int[6];
        for (int i = 0, j = 0; i < n.Length; i++)
        {
            if (!String.IsNullOrEmpty(n[i]))
            {
                w[j] = int.Parse(n[i]);
                j++;
            }
        }

        int r2 = w[1] - w[3];
        int r1 = w[0] - w[4];

        Console.WriteLine("{0} {1}",  r1, r2);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }
}
