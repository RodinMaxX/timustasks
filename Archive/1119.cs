﻿using System;
using System.IO;

class Task1119_2
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"3 2
3
1 1
3 2
1 2");
        var s = (input.ReadLine().Split(' '));
        int n = int.Parse(s[0]);
        int m = int.Parse(s[1]);
        int length = 100;
        int k = int.Parse(input.ReadLine());
        var b = new int[k+1];
        for (int i = 0; i < k; i++)
        {
            var c = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
            b[i]= (c[0] << 16) | c[1];
        }
        Array.Sort(b, 0, k);
        var r = new byte[2, m + 1];
        int q = 0;
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                int t = Math.Max(r[(i-1)%2,j],r[i%2,j-1]);
                if (b[q] == ((i<<16) | j))
                {
                    t = Math.Max(t,r[(i-1)%2,j-1]+1);
                    q++;
                }
                r[i%2, j] = (byte)t;
            }
        }
        Console.WriteLine((length * ((r[n%2, m] * (Math.Sqrt(2) - 2)) + ((n + m)))).ToString("f0"));
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
