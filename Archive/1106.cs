﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1106
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"4
2 0
1 4 0
4 0
2 3 0");

        var n = int.Parse(input.ReadLine());
        var a = new byte[n+1];
        var team1 = new List<int>(n);
        for (int i = 1; i <= n; i++)
        {
            bool iCanBeInTeam2 = false;

            foreach (var t in input.ReadLine().Split(' '))
            {
                int j = int.Parse(t);
                if (j>0 && a[j]==0)
                {
                    if (a[i] !=2)
                    {
                        a[j] = 2;
                    }
                    else
                    {
                        a[j] = 1;
                        team1.Add(j);
                    }
                }
                if (j>0 && a[j] == 1)
                {
                    iCanBeInTeam2 = true;
                }
            }
            if (a[i]==0)
            {
                if (iCanBeInTeam2) a[i] = 2;
                else
                {
                    a[i] = 1; 
                    team1.Add(i);
                }
            }
        }
       
        Console.WriteLine(team1.Count);
        Console.WriteLine(String.Join(" ", team1));
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}


