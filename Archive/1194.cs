﻿using System;
using System.IO;

class Task1194
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"3 1
1 2 2 2 3 1
2 2 4 1 5 1");

        var s = input.ReadLine().Split(' ');
        var n = int.Parse(s[0]);
        var k = int.Parse(s[1]);
        Console.WriteLine((n*(n-1))/2 -k);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
