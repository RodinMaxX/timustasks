﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace _1196
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input1.txt") : Console.In;
            var dict = new HashSet<int>(Enumerable.Repeat(1, int.Parse(input.ReadLine()))
                                                   .Select(_ => int.Parse(input.ReadLine())));

            Console.WriteLine(Enumerable.Repeat(1, int.Parse(input.ReadLine()))
                                        .Select(_ => int.Parse(input.ReadLine()))
                                        .Count(x => dict.Contains(x)));
        /*
            Console.WriteLine(Enumerable.Repeat(new HashSet<int>(Enumerable.Repeat(1, int.Parse(input.ReadLine()))
                                                                           .Select(_ => int.Parse(input.ReadLine()))), 
                                                int.Parse(input.ReadLine()))
                                        .Select(a => new { a, x = int.Parse(input.ReadLine())})
                                        .Count(p => p.a.Contains(p.x)));
        */
            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
    }
}
