﻿using System;
using System.IO;
using System.Linq;

class Task2035
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"2 2 4
");
        int[] a = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        int diff = a[2] - a[0];
        if (diff < 0)
        {
            Console.WriteLine(a[2] + " 0");
        }
        else if (diff <= a[1])
        {
            Console.WriteLine(a[0] + " " + diff);
        }
        else 
        {
            Console.WriteLine("Impossible");
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
