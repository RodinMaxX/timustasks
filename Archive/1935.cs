﻿using System;
using System.IO;
using System.Linq;

class Task1935
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"5
5 10 10 5 3
");
        input.ReadLine();
        int[] b = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        Console.WriteLine(b.Sum() + b.Max());
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
