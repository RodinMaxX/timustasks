﻿using System;
using System.IO;


class Task1197
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0
            ? new StringReader(
@"4
a1
d4
g6
a8
")
            : Console.In;
        var n = int.Parse(input.ReadLine());

        for (int i = 0; i < n; i++)
        {
            string s = input.ReadLine();
            var x = s[0] - 'a';
            if (x > 3) x = 7 - x;
            if (x > 2) x = 2;
            var y = s[1] - '1';
            if (y > 3) y = 7 - y;
            if (y > 2) y = 2;

            Console.WriteLine("23468"[x + y]);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }

}
