﻿using System;
using System.IO;

class Task1496
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
                @"11
naucoder
iceman
abikbaev
abikbaev
petr
abikbaev
abikbaev
x
abikbaev
acrush
x");
        int n = int.Parse(input.ReadLine());

        string[] a = new string[n];

        for (int i = 0; i < n; i++)
        {
            a[i] = input.ReadLine();
        }

        for (int i = 0; i < n; i++)
        {
            if (IsDuplic(a, i) && IsFirstRepeat(a, i))
            {
                Console.WriteLine(a[i]);
            }
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }  

    static bool IsDuplic(string[] a, int index)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (i != index && a[i] == a[index]) return true;
            }
            return false;
        }

        static bool IsFirstRepeat(string[] a, int index)
        {
            for (int i = 0; i < index; i++)
            {
                if (a[i] == a[index]) return false;
            }
            return true;
        }
    }
