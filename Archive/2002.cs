﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;

class Task2002
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"6
register vasya 12345
login vasya 1234
login vasya 12345
login anakin C-3PO
logout vasya
logout vasya
");
        int n = int.Parse(input.ReadLine());
        Dictionary<string,string> users = new Dictionary<string,string>();
        HashSet<string> sessions = new HashSet<string>();
        for (int i = 0; i < n; i++)
        {
            string[] s = input.ReadLine().Split(' ');
            var action = s[0];
            string login = s[1];
           // string password = s[2];
            if (action == "register")
            {
                if (!users.ContainsKey(login))
                {
                    users.Add(login, s[2]);
                    Console.WriteLine("success: new user added");
                }
                else
                {
                    Console.WriteLine("fail: user already exists");
                }
            }
            else if (action == "login")
            {
                if (!users.ContainsKey(login))
                {
                    Console.WriteLine("fail: no such user");
                }
               else if (users[login] != s[2])
                {
                    Console.WriteLine("fail: incorrect password");
                }
                else if (!sessions.Contains(login))
                {
                    sessions.Add(login);
                    Console.WriteLine("success: user logged in");
                }
                else
                {
                    Console.WriteLine("fail: already logged in");
                }
            }
            else if (action == "logout")
            {
                if (!users.ContainsKey(login))
                {
                    Console.WriteLine("fail: no such user");
                }
                else if (!sessions.Contains(login))
                {
                    Console.WriteLine("fail: already logged out");
                }
                else
                {
                    sessions.Remove(login);
                    Console.WriteLine("success: user logged out");
                }
            }
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

        