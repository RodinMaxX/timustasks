﻿using System;
using System.IO;

class Task2056
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"3
3
3
3
");
        int n = int.Parse(input.ReadLine());
        int sum = 0;
        int counter3 = 0;
        for (int i = 0; i < n; i++)
        {
            int x = int.Parse(input.ReadLine());
            if (x == 3) counter3++;
            sum += x;
        }
        int avg2 = (counter3 > 0) ? 0 : (sum * 2 / n) - 7;
        Console.WriteLine(("None,Common,High,Named".Split(','))[avg2]);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
