﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1005
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"5
5 8 13 27 14");

        int n = int.Parse(input.ReadLine());
        int[] b = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        var border = 1 << (n - 1);
        int sum = 0;
       
        for (int i = 0; i < n; i++)
        {
            sum += b[i];
        }
        
        int diffr = int.MaxValue; 
        for (int i = 0; i < border; i++)
        {
            int sum2=0; 
            for (int j = 0; j < n-1; j++)
            {
                if ((i & (1 << j)) != 0)
                {
                    sum2 += b[j];
                }
            }
            int diffrCur =  Math.Abs(sum - 2*sum2);
            if (diffrCur < diffr) diffr = diffrCur;
        }
        Console.WriteLine(diffr);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}



