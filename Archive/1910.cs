﻿using System;

namespace _1910
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            string s = Console.ReadLine();
            var a = s.Split(' ');
            int[] sum = new int[n];
            int smax;
            int[] b = new int[n];

            for (int i = 0; i < n; i++)
            {
                b[i] = int.Parse(a[i]);
            }

            for (int i = 2; i < n; i++)
            {
                sum[i] = b[i - 2] + b[i - 1] + b[i];
            }
            smax = sum[2];
            int numbmax = 2;
            
            for (int i = 3; i < n; i++)
            {
                if (sum[i] > smax)
                {
                    smax = sum[i];
                    numbmax = i;
                }
            }
            Console.WriteLine("{0} {1}", smax, numbmax);
        }
    }
}
