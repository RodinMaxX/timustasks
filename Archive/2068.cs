﻿using System;
using System.IO;

class Task2068
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"5
777 313 465 99 1
");
        int n = int.Parse(input.ReadLine());
        var s = input.ReadLine().Split(' ');
        int[] piles = new int [n];
  int allHods=0/* = (n - 1) / 2*/;
        for (int i = 0; i < n; i++)
        {
            piles[i] = int.Parse(s[i]);
            int hods = (piles[i] -1) /2;
            allHods += hods;
        }
        Console.WriteLine(allHods % 2 == 0 ? "Stannis" : "Daenerys");

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
