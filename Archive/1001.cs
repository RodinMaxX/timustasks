﻿using System;
using System.IO;
using System.Linq;

class Task1001
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(
            @" 1427  0   

   876652098643267843 
5276538
  
  ") : Console.In;
        var a = input.ReadToEnd().Split('\n', ' ', '\r');
        for (int i = a.Length - 1; i >= 0; i--)
        {
            if (!String.IsNullOrEmpty(a[i]))
            {
                Console.WriteLine((Math.Sqrt(Int64.Parse(a[i]))).ToString("f4"));
            }
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}