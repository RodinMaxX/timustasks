﻿using System;
using System.IO;

class Task1131
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"8 2400
");
        string[] s = (input.ReadLine()).Split(' ');
        int n = int.Parse(s[0]);
        int k = int.Parse(s[1]);
        int a =1;
        int hours = 0;
        while (a<=k && a*2<=n)
        {
            a = a * 2;
            hours++;
        }
       
        int b = n - a;
        if (b >= 0)
        {
            if (b % k != 0)  hours++;
            Console.WriteLine(hours + b / k);
}
        else
        {
            Console.WriteLine(hours);
        }

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
