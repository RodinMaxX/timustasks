﻿using System;
using System.IO;

namespace _1601
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input1.txt") : Console.In;
            bool isFirstLetter = true;
            while (input.Peek() >= 0)
            {
                string s = input.ReadLine();
                string r = s.ToLower();
                var a = r.ToCharArray();

                for (int i = 0; i < a.Length; i++)
                {
                    // Console.Write(a[i]);
                    if (char.IsLetter(a[i]) && isFirstLetter)
                    {
                        a[i] = char.ToUpper(a[i]);
                        isFirstLetter = false;
                    }

                    if (a[i] == '.' || a[i] == '!' || a[i] == '?')
                    {
                        isFirstLetter = true;
                    }
                }
                Console.WriteLine(a);
            }

            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
    }
}
    