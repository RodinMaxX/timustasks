﻿using System;

namespace _1567
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            string str = Console.ReadLine();

            for (int i = 0; i < str.Length; ++i)
            {
                sum += Search(str[i]);
            }

            Console.Write(sum);
        }

        static int Search(char v)
        {
            switch (v)
            {
                case 'a':
                case 'd':
                case 'g':
                case 'j':
                case 'm':
                case 'p':
                case 's':
                case 'v':
                case 'y':
                case '.':
                case ' ':
                    return 1;

                case 'b':
                case 'e':
                case 'h':
                case 'k':
                case 'n':
                case 'q':
                case 't':
                case 'w':
                case 'z':
                case ',':
                    return 2;

                case 'c':
                case 'f':
                case 'i':
                case 'l':
                case 'o':
                case 'r':
                case 'u':
                case 'x':
                case '!':
                    return 3;
            } 
            return 0;
        }
    }
}