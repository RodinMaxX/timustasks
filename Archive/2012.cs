﻿using System;

namespace _2012
{
    class Program
    {
        static void Main()
        {
            int f = int.Parse(Console.ReadLine());
            int t = 12 - f;
            int timeForTask = (5 - 1) * 60 / 45;
            Console.WriteLine(timeForTask >= t ? "YES" : "NO");
        }
    }
}
