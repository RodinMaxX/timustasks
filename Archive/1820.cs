﻿using System;
using System.IO;
using System.Linq;

class Task1820
{
    static void Main(string[] args)
    {
 
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(
@"3 2
"): Console.In;
       
        var s =(input.ReadLine().Split(' '));
        int n = int.Parse(s[0]);
        int k = int.Parse(s[1]);
        int m = ((n*2 - 1) / k) + 1;
        if (n < k) m = 2;
        Console.WriteLine(m);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }
}
