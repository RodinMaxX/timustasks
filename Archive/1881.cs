﻿using System;
using System.IO;

class Task1881
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"1 5 6
To
be
or
not
to
be");
        string[] a = input.ReadLine().Split(' ');
        var height = int.Parse(a[0]);
        int width = int.Parse(a[1]);
        int chars = 0;
        int lines = 0;
        for (var str = ""; str != null; str = input.ReadLine())
        {
            if (chars > 0 && str.Length > 0) chars++;
            chars = chars + str.Length;
            if (chars > width)
            {
                lines++;
                chars = str.Length;
            }
        }
        int result = lines / height + 1;
        Console.WriteLine(result);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
