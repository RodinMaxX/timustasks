﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class Task1925
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"6 5
3 1
3 1
5 3
6 5
5 5
7 2");

        var a = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        int n = a[0];
        int r =a[1]- 2;
        for (int i = 0; i < n; i++)
        {
            var b = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
            r -= b[1] - (b[0] - 2);
        }
        Console.WriteLine(r < 0 ? "Big Bang!" : r.ToString());
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
