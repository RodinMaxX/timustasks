﻿using System;
using System.IO;

class Task1880
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"5
13 20 22 43 146
4
13 22 43 146
5
13 43 67 89 146");
        input.ReadLine();
        int[] a = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        input.ReadLine();
        int[] b = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        input.ReadLine();
        int[] c = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);

        int counter = 0;
        foreach (var x in a)
        {
            if (Array.BinarySearch(b, x) >= 0 && Array.BinarySearch(c, x) >= 0) counter++;
        }

        Console.Write(counter);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
