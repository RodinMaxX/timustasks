﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace _1319
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0
                ? new StreamReader("../../input1.txt", Encoding.Default)
                : Console.In;

            int n = int.Parse(input.ReadLine());
            int[,] a = new int[n, n]; // i,j

            int t = 1;
            for (int p = n-1; p >=0; p--)
            {
                for (int j = p; j < n; j++)
                {
                   // Console.WriteLine("{0} {1} {2} {3}", t, j, k, i);
                    a[j-p, j] = t++;
                } //for k
            } // forj

            for (int p = 1; p < n; p++)
            {
                for (int i = p; i < n; i++)
                {
                   // Console.WriteLine("{0} {1} {2} {3}", t, p, i, j);
                    a[i, i-p] = t++;
                }   
            }
            
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(a[i,j]+" ");
                    
                } 
                Console.WriteLine();
            } 

            if (Array.IndexOf(args, "input") >= 0)
            {
                Console.WriteLine("Отладочный запуск завершён, нажми любую клавишу для выхода");
                Console.ReadKey();
            }
        } 
    }
}

    