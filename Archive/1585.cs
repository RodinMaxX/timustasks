﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Linq;

namespace _1585
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0 ? new StreamReader(@"..\..\input2.txt") : Console.In;
            //Console.WriteLine(input.ReadToEnd().Split('\n').Skip(1).ToLookup(x => x).OrderBy(x => x.Count()).Last().Key);
            int n = int.Parse(input.ReadLine());
            int[] counter = new int[3];
            string[] etalon = {"Emperor Penguin","Macaroni Penguin","Little Penguin"};
            

            for (int i = 0; i < n; i++)
            {
               int a = Array.IndexOf(etalon, input.ReadLine());
                if (a >= 0)
                {
                    counter[a]++;
                }
            }
            int max=counter.Max();
            int penguinNumber= Array.IndexOf(counter,max);
            Console.WriteLine(etalon[penguinNumber]);
            
            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }
    }
}
