﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1370_real
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"12 7
1
2
3
4
5
6
7
8
9
0
7
7");
       
        var a = input.ReadLine().Split(' ');
        int n = int.Parse(a[0]);
        int m = int.Parse(a[1]);

        string[] symbs = new string[n];
        for (int i = 0; i < n; i++)
        {
            symbs[i] = input.ReadLine();
        }

        for (int i =0, j = m%n; i < 10; i++, j++)
        {
            if (j == n) j = 0;
            Console.Write(symbs[j]);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

