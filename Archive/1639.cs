﻿using System;
using System.IO;

class Task1639
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"2 4");

        var a = Array.ConvertAll((input.ReadLine()).Split(' '), int.Parse);
        Console.WriteLine(("[:=[first],[second]=:]").Split(',')[(a[0] & a[1] & 1)]);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
