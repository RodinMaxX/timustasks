﻿using System;
using System.IO;

class Task1617
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"1
601
");

        int n = int.Parse(input.ReadLine());
        int[] counters = new int [101];
        int wagons = 0;
        while (n-- > 0)
        {
            var x = int.Parse(input.ReadLine());
            if ((++counters[x - 600]) % 4 == 0) wagons++;
        }
        Console.WriteLine(wagons);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
