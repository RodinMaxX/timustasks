﻿using System;
using System.IO;
using System.Linq;

class Task1893
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"21F
");
        string s = (input.ReadLine());
        var col = s.Last();
        var row = int.Parse(s.Substring(0, s.Length - 1));
        string answer= "aisle";
        if (row <= 20)
        {
            if (col == 'A' || col == ((row <= 2) ? 'D' : 'F')) answer = "window";
        }
        else
        {
            if (col == 'A' || col == 'K') answer = "window";
            else if ( !"CDGH".Contains(col)) answer = "neither";
        }
        Console.WriteLine(answer);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
