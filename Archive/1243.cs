﻿using System;
using System.IO;

class Task1243
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"1234561234566546546545645646465487897987564564564654654654654
");
        var n = (input.ReadLine());
        int r = 0;
        for (int i = 0; i < n.Length; i++)
        {
            r = (r *10+ ( n[i]- '0')) % 7;
        }
        Console.WriteLine(r);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
