﻿using System;
using System.IO;
using System.Linq;

class Task1119
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"3
4
1
6");
        int n = int.Parse(input.ReadLine());
        int[] inp = new int[n];
        for (int i = 0; i < n; i++)
        {
            inp[i] = int.Parse(input.ReadLine());
        }
        Array.Sort(inp);

        for (int i =inp.Length - 1; i >= 0; i--)
        {
            Console.WriteLine(inp[i]);
        }

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
