﻿using System;
using System.IO;
using System.Linq;

class Task1876
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"40 40
");
        var s = input.ReadLine().Split(' ');
        int a = int.Parse(s.First());
        var b = int.Parse(s.Last());
        int r;
        r = Math.Max(2 * b + 40, 2 * 39 + 40 + (a - 40) * 2+1);
        Console.Write(r);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

        