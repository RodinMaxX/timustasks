﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1180
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"374545465446554879879798756454261321654687889494565");
        var n = input.ReadLine();
        int sum = 0;

        foreach (var a in n)
        {
            int t = int.Parse(a.ToString());
            sum += t;
        }
        if (sum%3 == 0)
        {
            Console.WriteLine("2");
        }
        else
        {
            Console.WriteLine("1");
            Console.WriteLine(sum%3);
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

