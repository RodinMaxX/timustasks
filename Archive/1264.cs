﻿using System;
using System.IO;
using System.Linq;

class Task1264
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") >= 0 ? new StringReader(@"3 1") : Console.In;
        string s = input.ReadLine();
        var n = s.Split(' ');
        int a = int.Parse (n[0]);
        int b = int.Parse(n[1]);
        int r = a * (b + 1);
        Console.WriteLine(r);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey(); 
    }
}
