﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1446
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"7
Ivan Ivanov
Gryffindor
Mac Go Nagolo
Hufflepuff
Zlobeus Zlei
Slytherin
Um Bridge
Slytherin
Tatiana Henrihovna Grotter
Ravenclaw
Garry Potnyj
Gryffindor
Herr Mionag-Ranger
Gryffindor");
        int n = int.Parse(input.ReadLine());
               
        var a = new Dictionary<object, List<string>>();
        for (int i = 0; i < n; i++)
        {
              var name = input.ReadLine();
              object faculty = String.Intern(input.ReadLine());
            List<string> b;

            if (!a.TryGetValue(faculty,out b))
            {
                b = new List<string>();
                a.Add(faculty, b);
            }
            b.Add(name);
            }

        foreach (var faculty in new[] { "Slytherin","Hufflepuff","Gryffindor","Ravenclaw" })
        {
            Console.WriteLine(faculty + ":");
            foreach (var name in a[String.Intern(faculty)])
            {
                Console.WriteLine(name);
            }
            Console.WriteLine();
        }
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}


