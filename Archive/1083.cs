﻿using System;
using System.IO;
using System.Linq;

class Task1083
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"8 !
");
        var s = input.ReadLine().Split(' ');
        int n = int.Parse(s.First());
        var k = s.Last().Length;
        int res=1;
        for (int i = n; i>1; i-=k)
        {
            res *= i;
        }
        Console.Write(res);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

        