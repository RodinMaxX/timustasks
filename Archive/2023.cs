﻿using System;

namespace _2023
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            int sum = 0;
            int t0 = 2;
            for (int i = 0; i < n; i++)
            {
                string name = Console.ReadLine();
                int t = BoxNumber(name[0]);
                int dist = Math.Abs(t - t0);
                sum = sum + dist;
                // Console.WriteLine("t={0}, t0={3}, dist={1}, sum = {2}",t,dist, sum, t0);
                t0 = t;
            }
            Console.WriteLine(sum);
        }

        static int BoxNumber(char v)
        {
            return ("BMS-APOR".IndexOf(v) + 4) / 4;
        }
    }

}
