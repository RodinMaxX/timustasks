﻿using System;
using System.IO;

class Task1025
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"1
3
");
        int k = int.Parse(input.ReadLine());
        int[] p = Array.ConvertAll((input.ReadLine()).Split(' '),int.Parse);
        Array.Sort(p);

        int counter = 0;
        for (int i = k/2; i>=0; i--)
        {
            counter += p[i]/2 + 1;
        }
            Console.WriteLine(counter);

        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
