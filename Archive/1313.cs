﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") >= 0
                ? new StreamReader("../../input2.txt", Encoding.Default)
                : Console.In;

            int n = int.Parse(input.ReadLine());
            string[,] a = new string[n, n]; // i,j

            for (int i = 0; i < n; i++)
            {
                string[] s = input.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    a[i, j] = s[j];
                } //for k

            } // forj


            for (int k = 0; k < n; k++)
            {
                int i = k;
                for (int j = 0; j <= k; j++)
                {
                    // Console.WriteLine(i-- + " " + j); 
                    Console.Write(a[i--, j] + " ");
                }


            }

            for (int k = 1; k < n; k++)
            {
                int i = n - 1;
                for (int j = k; j < n; j++)
                {
                    // Console.WriteLine(i-- + " " + j); 
                    Console.Write(a[i--, j] + " ");
                }
            }
            Console.WriteLine();

            if (Array.IndexOf(args, "input") >= 0)
            {
                Console.WriteLine("Отладочный запуск завершён, нажми любую клавишу для выхода");
                Console.ReadKey();
            }
        } // main
    } // class
} // namespace
