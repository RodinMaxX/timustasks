﻿using System;
using System.IO;

class Task2066
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0 ? Console.In : new StringReader(
@"1
2
3
");
        var a = int.Parse(input.ReadLine());
        var b = int.Parse(input.ReadLine());
        var c = int.Parse(input.ReadLine());

        Console.WriteLine(a - (b<2 ? b+c : b*c));
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
