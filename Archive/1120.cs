﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1120
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
                              @"1000000000");

        int n, p;
        int a = 0;
        n = int.Parse(input.ReadLine());
        for (p = (int)(Math.Sqrt(1 + 8 * (long)n) / 2); p > 0; p--)
        {
            var t = 2*n - p*p + p;
            if(t%(2*p) == 0)
            {
                a = t/(2*p);
                break;
            }
        }
        Console.WriteLine("{0} {1}", a, p);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    } 
}

