﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class Task1636
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"300 719
0 0 0 0 0 0 21 0 0 0");

        var a = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        int counter = (Array.ConvertAll(input.ReadLine().Split(' '),int.Parse)).Sum();
        Console.WriteLine(20 * counter <= (a[1] - a[0]) ? "No chance." : "Dirty debug :(");
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}
