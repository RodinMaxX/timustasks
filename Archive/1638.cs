﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

class Task1638
{
    static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
            ? Console.In
            : new StringReader(
@"10 1 1 3
");
        //int[] w = new[] {int.Parse(s[0]), int.Parse(s[1]), int.Parse(s[2]), int.Parse(s[3])};
        int[] w = Array.ConvertAll(input.ReadLine().Split(' '), int.Parse);
        if (w[3] > w[2])
        {
            int r = w[1]*2 *(w[3]-w[2])  + (w[3] - w[2] - 1) * w[0];
            Console.WriteLine(r);
        }
        else if (w[3] < w[2])
        {
            int r = w[1]*2 * (w[2] - w[3] ) + (w[2] - w[3] + 1) * w[0];
            Console.WriteLine(r);
        }
        else Console.WriteLine(w[0]);
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

        