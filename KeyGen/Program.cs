﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace KeyGen
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Array.IndexOf(args, "input") < 0
                ? Console.In
                : new StringReader(
@"Mirmal 262
");
            var s = input.ReadLine().Split(' ');
            string user = s[0];
            string lic = s[1];
            lic = user.GetHashCode().ToString() /*GenerateNew(user)*/; Console.WriteLine(lic);
            Console.WriteLine(IsValid(user, lic) ? "Спасибо за покупку!" : "Проверьте правильность ввода");
            if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
        }

        public static bool IsValid(string user, string lic)
        {
            int n;
            if (int.TryParse(lic, NumberStyles.HexNumber, null, out n) && user.Sum(x => (int)x) == n) return true;
            else return false;   
        }

        private static string GenerateNew(string user)
        {
            return (user.Sum(x => (int) x).ToString("X"));
        }
    }
}

