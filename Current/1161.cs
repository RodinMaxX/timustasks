﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

internal class Task1161
{
    private static void Main(string[] args)
    {
        var input = Array.IndexOf(args, "input") < 0
                        ? Console.In
                        : new StringReader(
@"3
30
72
50");
        int n = int.Parse(input.ReadLine());
        int[] a = new int[n];
        for (int i = 0; i < n; i++)
        {
            a[i] = int.Parse(input.ReadLine());
        }

        Array.Sort(a);
        float res = a[n-1];
        for (int i = n-2; i >= 0; i--)
        {
            res = 2*((float)Math.Sqrt(res*a[i]));
        }

        Console.WriteLine(res.ToString("f2"));
        if (Array.IndexOf(args, "input") >= 0) Console.ReadKey();
    }
}

